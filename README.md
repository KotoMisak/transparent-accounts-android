#Transparent accounts viewer
![alt text](./app/src/main/res/mipmap-xxxhdpi/ic_launcher.png "KoTiLogo")

Aim of this project is to provide information about transparent accounts from the bank open API.

Stay tuned, details are coming soon...

# Current screens

![Account list](./scr/account_list.png)

![Account list](./scr/account_sq_list.png)

![Account detail](./scr/account_detail.png)

