package cz.koto.misak.transaccounts.rest;

import java.util.List;

import cz.koto.misak.transaccounts.model.Account;
import cz.koto.misak.transaccounts.model.TransactionContainer;
import retrofit.http.GET;
import rx.Observable;

public interface CSClient {

    @GET("/api/kotinode/account")
    Observable<List<Account>> accounts();

    @GET("/api/kotinode/account")
    List<Account> getEvents();


    @GET("/api/kotinode/transaction")
    Observable<TransactionContainer> transactionContainer();

    @GET("/api/kotinode/transaction")
    TransactionContainer getTransactionContainer();
}
