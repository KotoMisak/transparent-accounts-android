package cz.koto.misak.transaccounts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.List;

import cz.koto.misak.transaccounts.detail.DetailActivity;
import cz.koto.misak.transaccounts.list.AccountListAdapter;
import cz.koto.misak.transaccounts.model.Account;
import cz.koto.misak.transaccounts.rest.AccountApi;
import cz.koto.misak.transaccounts.rest.CSClient;
import cz.koto.misak.transaccounts.utils.RxUtils;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class AccountListActivity extends Activity {

    private Toolbar toolbar;

    private RecyclerView recyclerView;
    private StaggeredGridLayoutManager staggeredLayoutManager;
    private AccountListAdapter adapter;
    private boolean isListView;
    private Menu menu;
    private CSClient csClient;
    private CompositeSubscription _subscriptions = new CompositeSubscription();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_list_activity);

        toolbar = (Toolbar) findViewById(R.id.accListToolbar);
        setUpActionBar();

        staggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);

        recyclerView = (RecyclerView) findViewById(R.id.list);
        recyclerView.setLayoutManager(staggeredLayoutManager);

        recyclerView.setHasFixedSize(true); //Data size is fixed - improves performance
        adapter = new AccountListAdapter(this);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(onItemClickListener);

        isListView = true;

        csClient = new AccountApi().getCSClient(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        loadCSAccounts();
    }

    @Override
    public void onResume() {
        super.onResume();
        _subscriptions = RxUtils.getNewCompositeSubIfUnsubscribed(_subscriptions);
    }

    @Override
    public void onPause() {
        super.onPause();
        RxUtils.unsubscribeIfNotNull(_subscriptions);
    }

    AccountListAdapter.OnItemClickListener onItemClickListener = new AccountListAdapter.OnItemClickListener() {
        @Override
        public void onItemClick(View v, int position) {
            Intent transitionIntent = new Intent(AccountListActivity.this, DetailActivity.class);
            transitionIntent.putExtra(DetailActivity.EXTRA_PARAM_DETAIL, adapter.getAccount(position));
            ImageView accountImage = (ImageView) v.findViewById(R.id.accountImage);
            TextView accountNumber = (TextView) v.findViewById(R.id.accountNumber);
            TextView accountBalance = (TextView) v.findViewById(R.id.accountBalance);
            TextView accountName = (TextView) v.findViewById(R.id.accountName);
            TextView accountDesc = (TextView) v.findViewById(R.id.accountDescription);

            Pair<View, String> imagePair = Pair.create((View) accountImage, "tImage");
            Pair<View, String> numberPair = Pair.create((View) accountNumber, "tAccNumber");
            Pair<View, String> balancePair = Pair.create((View) accountBalance, "tAccBalance");
            Pair<View, String> toolbarPair = Pair.create((View) toolbar, "tActionBar");
            Pair<View, String> namePair = Pair.create((View) accountName, "tAccName");
            Pair<View, String> descPair = Pair.create((View) accountDesc, "tAccDesc");


            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(AccountListActivity.this, imagePair, numberPair, balancePair, toolbarPair, namePair, descPair);
            ActivityCompat.startActivity(AccountListActivity.this, transitionIntent, options.toBundle());
        }
    };

    private void setUpActionBar() {
        if (toolbar != null) {
            setActionBar(toolbar);
            getActionBar().setDisplayHomeAsUpEnabled(false);
            getActionBar().setDisplayShowTitleEnabled(true);
            getActionBar().setElevation(7);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_toggle) {
            toggle();
            return true;
        }else if (id == R.id.action_search){
            Snackbar.make(recyclerView, getBaseContext().getResources().getText(R.string.account_search_not_implemented), Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void toggle() {
        MenuItem item = menu.findItem(R.id.action_toggle);
        if (isListView) {
            staggeredLayoutManager.setSpanCount(2);
            item.setIcon(R.drawable.ic_action_list);
            item.setTitle(getResources().getString(R.string.show_list));
            isListView = false;
        } else {
            staggeredLayoutManager.setSpanCount(1);
            item.setIcon(R.drawable.ic_action_grid);
            item.setTitle(getResources().getString(R.string.show_grid));
            isListView = true;
        }
    }

    private void loadCSAccounts() {
        if (csClient == null) return; //just for mock flavour
        Observable<List<Account>> events = csClient.accounts();
        _subscriptions.add(
                events
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<List<Account>>() {
                            @Override
                            public void onCompleted() {

                                Log.d("-", "Retrofit call successful");
                                adapter.notifyItemInserted(adapter.getItemCount());
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.i("Api call failed", null, e);
                                Toast.makeText(getBaseContext(), getBaseContext().getResources().getText(R.string.connection_account_lost), Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onNext(List<Account>
                                                       eventList) {
                                for (Account account : eventList) {
                                    adapter.addAccount(account);
                                }
                            }
                        }));
    }

}