package cz.koto.misak.transaccounts.detail;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cz.koto.misak.transaccounts.R;
import cz.koto.misak.transaccounts.list.AccountListAdapter;
import cz.koto.misak.transaccounts.model.Transaction;
import cz.koto.misak.transaccounts.model.TransactionContainer;
import cz.koto.misak.transaccounts.rest.AccountApi;
import cz.koto.misak.transaccounts.rest.CSClient;
import cz.koto.misak.transaccounts.utils.TransitionAdapter;
import cz.koto.misak.transaccounts.model.Account;
import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class DetailActivity extends Activity implements View.OnClickListener {

    public static final String EXTRA_PARAM_DETAIL = "account_detail";
    public static final String NAV_BAR_VIEW_NAME = Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME;
    private ListView list;
    private ImageView imageView;
    private TextView accountNumber;
    private TextView accountBalance;
    private TextView accountName;
    private TextView accountDesc;
    private TextView accountNote;
    private TextView accountIban;
    private Palette palette;
    private ImageButton addButton;
    private Animatable animatable;
    private boolean isEditTextVisible;
    private Account account;
    private ArrayList<String> todoList;
    int defaultColorForRipple;

    private LinearLayout imageHolder;

    private CompositeSubscription _subscriptions = new CompositeSubscription();
    private CSClient csClient;
    private TransactionListAdapter transactionListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_detail_activity);

        Bundle data = getIntent().getExtras();
        account = data.getParcelable(EXTRA_PARAM_DETAIL);

        imageView = (ImageView) findViewById(R.id.accountImage);
        accountNumber = (TextView) findViewById(R.id.accountNumber);
        accountBalance = (TextView) findViewById(R.id.accountBalance);
        accountName = (TextView) findViewById(R.id.accountName);
        accountDesc = (TextView) findViewById(R.id.accountDescription);
        accountNote = (TextView) findViewById(R.id.accountNote);
        accountIban = (TextView) findViewById(R.id.accountIban);
        addButton = (ImageButton) findViewById(R.id.btnSearch);

        addButton.setImageResource(R.drawable.icn_morph_reverse);
        addButton.setOnClickListener(this);
        defaultColorForRipple = getResources().getColor(R.color.primary_dark);
        isEditTextVisible = false;

        imageHolder = (LinearLayout) findViewById(R.id.mainHolder);
        imageHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        csClient = new AccountApi().getCSClient(this);
        RecyclerView rv = (RecyclerView) findViewById(R.id.transaction_list);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(transactionListAdapter = new TransactionListAdapter());

        setAccount();
        windowTransition();
        setPhoto();

    }

    @Override
    public void onStart() {
        super.onStart();
        loadCSTransactions();
    }

    private void setAccount() {
        accountNumber.setText(account.formatAccountNumber());
        accountBalance.setText(account.formatBalance());
        accountDesc.setText(account.getDescription());
        accountName.setText(account.getName());
        accountNote.setText(account.getNote());
        accountIban.setText(account.getIban());
        imageView.setImageResource(account.getImageResourceId(this));
    }

    private void windowTransition() {
        getWindow().setEnterTransition(makeEnterTransition());
        getWindow().getEnterTransition().addListener(new TransitionAdapter() {
            @Override
            public void onTransitionEnd(Transition transition) {
                addButton.animate().alpha(1.0f);
                getWindow().getEnterTransition().removeListener(this);
            }
        });
    }

    public static Transition makeEnterTransition() {
        Transition fade = new Fade();
        fade.excludeTarget(android.R.id.navigationBarBackground, true);
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        return fade;
    }

    private void addToDo(String todo) {
        todoList.add(todo);
    }

    private void setPhoto() {
        Bitmap photo = BitmapFactory.decodeResource(getResources(), account.getImageResourceId(this));
        colorize(photo);
    }

    private void colorize(Bitmap photo) {
        palette = Palette.generate(photo);
        applyPalette();
    }

    private void applyPalette() {
        getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.primary_light)));

        // titleHolder.setBackgroundColor(palette.getMutedColor(getResources().getColor(R.color.secondary_light)));
        // titleHolder.setBackgroundColor(getResources().getColor(R.color.primary_text));

        applyRippleColor(palette.getVibrantColor(defaultColorForRipple),
                palette.getDarkVibrantColor(defaultColorForRipple));
    }

    private void applyRippleColor(int bgColor, int tintColor) {
        colorRipple(addButton, bgColor, tintColor);
    }

    private void colorRipple(ImageButton id, int bgColor, int tintColor) {
        View buttonView = id;
        RippleDrawable ripple = (RippleDrawable) buttonView.getBackground();
        GradientDrawable rippleBackground = (GradientDrawable) ripple.getDrawable(0);
        rippleBackground.setColor(bgColor);
        ripple.setColor(ColorStateList.valueOf(tintColor));
    }

    @Override
    public void onClick(View v) {
        Snackbar.make(v, getBaseContext().getResources().getText(R.string.transaction_search_not_implemented), Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    private void revealEditText(LinearLayout view) {
        int cx = view.getRight() - 30;
        int cy = view.getBottom() - 60;
        int finalRadius = Math.max(view.getWidth(), view.getHeight());
        Animator anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, finalRadius);
        view.setVisibility(View.VISIBLE);
        isEditTextVisible = true;
        anim.start();
    }

    private void hideEditText(final LinearLayout view) {
        int cx = view.getRight() - 30;
        int cy = view.getBottom() - 60;
        int initialRadius = view.getWidth();
        Animator anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, 0);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setVisibility(View.INVISIBLE);
            }
        });
        isEditTextVisible = false;
        anim.start();
    }

    @Override
    public void onBackPressed() {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(100);
        addButton.startAnimation(alphaAnimation);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                addButton.setVisibility(View.GONE);
                finishAfterTransition();
                transactionListAdapter = null;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void loadCSTransactions() {
        if (csClient == null) return; //just for mock flavour
        Observable<TransactionContainer> events = csClient.transactionContainer();
        _subscriptions.add(
                events
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<TransactionContainer>(){
                            @Override
                            public void onCompleted() {

                                Log.d("-", "Retrofit call successful");
                                transactionListAdapter.notifyItemInserted(transactionListAdapter.getItemCount());
                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.i("Api call failed", null, e);
                                Toast.makeText(getBaseContext(), getBaseContext().getResources().getText(R.string.connection_transaction_lost), Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onNext(TransactionContainer
                                                       container) {
                                if (container!=null) {
                                    for (Transaction transaction : container.getTransactions()) {
                                        transactionListAdapter.addTransaction(transaction);
                                    }

                                }
                            }
                        }));
    }
}
