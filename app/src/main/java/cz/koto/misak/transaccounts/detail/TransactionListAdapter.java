package cz.koto.misak.transaccounts.detail;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cz.koto.misak.transaccounts.R;
import cz.koto.misak.transaccounts.model.Transaction;

public class TransactionListAdapter extends RecyclerView.Adapter<TransactionViewHolder> {
    private List<Transaction> transactionList = new ArrayList<>();

    public TransactionListAdapter() {
    }

    TransactionListAdapter(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    @Override
    public TransactionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        final LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        final View v = layoutInflater.inflate(R.layout.transaction_card, viewGroup, false);
        return new TransactionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TransactionViewHolder transactionViewHolder, int i) {
        Transaction trn = transactionList.get(i);
        String amount = trn.getAmount()==null?"0":String.valueOf(trn.getAmount().getValue());
        String currency = trn.getAmount()==null?"":trn.getAmount().getCurrency();
        transactionViewHolder.transAmountCurrency.setText(currency.trim().length()>0?amount+currency:"-");
        transactionViewHolder.transSenderName.setText(trn.getSenderName());
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }

    public void addTransaction(Transaction transaction){
        this.transactionList.add(transaction);
    }
}