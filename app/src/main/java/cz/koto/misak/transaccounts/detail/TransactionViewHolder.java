package cz.koto.misak.transaccounts.detail;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import cz.koto.misak.transaccounts.R;

class TransactionViewHolder extends RecyclerView.ViewHolder {
    TextView transAmountCurrency;
    TextView transSenderName;

    TransactionViewHolder(View itemView) {
        super(itemView);
        transAmountCurrency = (TextView) itemView.findViewById(R.id.trans_amount_currency);
        transSenderName = (TextView) itemView.findViewById(R.id.trans_sender_name);
    }
}