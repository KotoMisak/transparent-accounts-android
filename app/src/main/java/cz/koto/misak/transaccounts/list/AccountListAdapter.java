package cz.koto.misak.transaccounts.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import cz.koto.misak.transaccounts.R;
import cz.koto.misak.transaccounts.model.Account;

public class AccountListAdapter extends RecyclerView.Adapter<AccountListAdapter.ViewHolder> {

    Context context;
    OnItemClickListener itemClickListener;
    List<Account> accountList = new ArrayList<>();

    private static final int EMPTY_VIEW = 10;

    public AccountListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_list_item, parent, false);
            return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Account account = accountList.get(position);

        holder.accountBalance.setText(account.formatBalance());
        holder.accountNumber.setText(account.formatAccountNumber());
        holder.accountName.setText(account.getName());
        holder.accountDescription.setText(account.getDescription());
        Picasso.with(context).load(account.getImageResourceId(context)).into(holder.accountImage);

    }

    @Override
    public int getItemCount() {
        return this.accountList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (this.accountList.size() == 0) {
            return EMPTY_VIEW;
        }
        return super.getItemViewType(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder /*extends EmptyViewHolder*/ implements View.OnClickListener {
        public LinearLayout accountHolder;
       // public LinearLayout accountNameHolder;
        public TextView accountBalance;
        public ImageView accountImage;
        public TextView accountNumber;
        public TextView accountName;
        public TextView accountDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            accountHolder = (LinearLayout) itemView.findViewById(R.id.mainHolder);
            accountBalance = (TextView) itemView.findViewById(R.id.accountBalance);
            //accountNameHolder = (LinearLayout) itemView.findViewById(R.id.accountNameHolder);
            accountImage = (ImageView) itemView.findViewById(R.id.accountImage);
            accountNumber = (TextView) itemView.findViewById(R.id.accountNumber);
            accountName = (TextView) itemView.findViewById(R.id.accountName);
            accountDescription = (TextView) itemView.findViewById(R.id.accountDescription);
            accountHolder.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(itemView, getPosition());
            }
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.itemClickListener = mItemClickListener;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public Account getAccount(int position) {
        return this.accountList.get(position);
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

    public void addAccount(Account account) {
        this.accountList.add(account);
    }
}
