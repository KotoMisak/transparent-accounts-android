package cz.koto.misak.transaccounts.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import java.math.BigDecimal;
import java.util.Date;

import cz.koto.misak.transaccounts.R;

public class Account implements Parcelable{

    private String accBankCode;
    private Date transparencyFrom;//2015-01-24
    private Date transparencyTo;//2015-01-24
    private Date publicationTo;//3000-01-01
    private Date actualizationDate;//2015-11-08T19:00:53.000+0000
    private BigDecimal balance;
    private String currency;
    private String name;
    private String description;
    private String note;
    private String iban;
    private String accNumber;
    public String imageName;

    public Account(Parcel in){
        this.accBankCode = in.readString();
        this.transparencyFrom = new Date(in.readLong());
        this.transparencyTo = new Date(in.readLong());
        this.publicationTo = new Date(in.readLong());
        this.actualizationDate = new Date(in.readLong());
        String balanceString = in.readString();
        this.balance = new BigDecimal(balanceString==null?"0":balanceString);
        this.currency = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.note = in.readString();
        this.iban = in.readString();
        this.accNumber = in.readString();
        this.imageName = in.readString();

    }

    public int getImageResourceId(Context context) {

        String displayConfigLocation = context.getResources().getString(R.string.config_display_location);
        String level1_image = context.getResources().getString(R.string.balance_level1_image);
        int ret = context.getResources().getIdentifier(level1_image, displayConfigLocation , context.getPackageName());

        if (balance == null) return ret;
        if (balance.floatValue() > context.getResources().getInteger(R.integer.balance_level3_value)){
            String level2_image = context.getResources().getString(R.string.balance_level3_image);
            ret = context.getResources().getIdentifier(level2_image, displayConfigLocation , context.getPackageName());
        }else if (balance.floatValue() > context.getResources().getInteger(R.integer.balance_level2_value)) {
            String level3_image = context.getResources().getString(R.string.balance_level2_image);
            ret = context.getResources().getIdentifier(level3_image, displayConfigLocation , context.getPackageName());
        }
        return ret;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(accBankCode);
        dest.writeLong(transparencyFrom==null?0:transparencyFrom.getTime());
        dest.writeLong(transparencyTo == null ? 0 : transparencyTo.getTime());
        dest.writeLong(publicationTo == null ? 0 : publicationTo.getTime());
        dest.writeLong(actualizationDate == null ? 0 : actualizationDate.getTime());
        dest.writeString(balance == null ? "0" : balance.toPlainString());
        dest.writeString(currency);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(note);
        dest.writeString(iban);
        dest.writeString(accNumber);
        dest.writeString(imageName);
    }

    public static final Parcelable.Creator<Account> CREATOR
            = new Parcelable.Creator<Account>() {

        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        public Account[] newArray(int size) {
            return new Account[size];
        }
    };



    public String getAccBankCode() {
        return accBankCode;
    }

    public void setAccBankCode(String accBankCode) {
        this.accBankCode = accBankCode;
    }

    public Date getTransparencyFrom() {
        return transparencyFrom;
    }

    public void setTransparencyFrom(Date transparencyFrom) {
        this.transparencyFrom = transparencyFrom;
    }

    public Date getTransparencyTo() {
        return transparencyTo;
    }

    public void setTransparencyTo(Date transparencyTo) {
        this.transparencyTo = transparencyTo;
    }

    public Date getPublicationTo() {
        return publicationTo;
    }

    public void setPublicationTo(Date publicationTo) {
        this.publicationTo = publicationTo;
    }

    public Date getActualizationDate() {
        return actualizationDate;
    }

    public void setActualizationDate(Date actualizationDate) {
        this.actualizationDate = actualizationDate;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String formatBalance(){
        StringBuilder ret = new StringBuilder();
        if (getCurrency()==null) {
            ret.append("-");
        }else {
            ret.append(getBalance()==null?"0":String.valueOf(getBalance()));
            ret.append(getCurrency());
        }
        return ret.toString();
    }

    public String formatAccountNumber(){
        if (this.accBankCode == null) return "";
        if (this.accNumber == null) return "";
        return getAccNumber()+"/"+getAccBankCode();
    }
}
