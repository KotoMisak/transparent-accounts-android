package cz.koto.misak.transaccounts.model;


import java.math.BigDecimal;

public class Amount {

    private BigDecimal value;
    private Integer precision;
    private String currency;

    public Amount() {
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Integer getPrecision() {
        return precision;
    }

    public void setPrecision(Integer precision) {
        this.precision = precision;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
