package cz.koto.misak.transaccounts.model;


public class Sender {

    private String variableSymbol;
    private String constantSymbol;
    private String description;
    private String specificSymbol;
    private String specificSymbolParty;

    public Sender() {
    }

    public String getVariableSymbol() {
        return variableSymbol;
    }

    public void setVariableSymbol(String variableSymbol) {
        this.variableSymbol = variableSymbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getConstantSymbol() {
        return constantSymbol;
    }

    public void setConstantSymbol(String constantSymbol) {
        this.constantSymbol = constantSymbol;
    }

    public String getSpecificSymbol() {
        return specificSymbol;
    }

    public void setSpecificSymbol(String specificSymbol) {
        this.specificSymbol = specificSymbol;
    }

    public String getSpecificSymbolParty() {
        return specificSymbolParty;
    }

    public void setSpecificSymbolParty(String specificSymbolParty) {
        this.specificSymbolParty = specificSymbolParty;
    }
}
