package cz.koto.misak.transaccounts.rest;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.GsonBuilder;

import cz.koto.misak.transaccounts.R;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

public class AccountApi {

    public CSClient getCSClient(Context context) {
        Resources rsc = context.getResources();
        String CS_API_URL = rsc.getString(R.string.cs_api_url);
        if ((CS_API_URL == null)||(CS_API_URL.trim().length() == 0)) return null;//mock flavour
        //GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'");//ISO-8601
        GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("yyyy-MM-dd");//.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(CS_API_URL)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())//important for RX!!!
                .build();

        return retrofit.create(CSClient.class);
    }
}
